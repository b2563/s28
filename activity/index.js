//objective 1 (InsertOne Method)
db.hotel.insertOne({
	"name": "single",
	"accomodates": 2,
	"price": 1000,
	"description": "A simple room with all the basic necessities",
	"roomsAvailable": 10,
	"isAvailable": false
});

//objective 2 (InsertMany Method)
db.hotel.insertMany([
	{
	"name": "double",
	"accomodates": 3,
	"price": 2000,
	"description": "A room fit for a small family going on a vacation",
	"roomsAvailable": 5,
	"isAvailable": false		
	},
	{
	"name": "queen",
	"accomodates": 4,
	"price": 4000,
	"description": "A room with a queen sized bed perfect for a simple getaway",
	"roomsAvailable": 15,
	"isAvailable": false		
	}
]);

//objective 3 (Find Method)

db.hotel.find({
	"name": "double"
});

//objective 4 (UpdateOne Method)

db.hotel.updateOne(
	{
		"name": "queen"
	},
	{
		$set: {
			"roomsAvailable": 0
		}
	});

//objective 5 (DeleteMany Method)

db.hotel.deleteMany({
	"roomsAvailable": 0
});
