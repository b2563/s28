// CRUD Operations
/*
    - CRUD operations are the heart of any backend application.
    - Mastering the CRUD operations is essential for any developer.
    - This helps in building character and increasing exposure to logical statements that will help us manipulate our data.
    - Mastering the CRUD operations of any language makes us a valuable developer and makes the work easier for us to deal with huge amounts of information.
*/

// [Section] Inserting documents (Create)

// Insert one document
/*
    - Since mongoDB deals with objects as it's structure for documents, we can easily create them by providing objects into our methods.
    - The mongo shell also uses JavaScript for it's syntax which makes it convenient for us to understand it's code
    - Creating MongoDB syntax in a text editor makes it easy for us to modify and create our code as opposed to typing it directly in the terminal where the whole code is only visible in one line.
    - By using a text editor it allows us to type the syntax using multiple lines and simply copying and pasting the code in terminal will make it work.
    - Syntax
        - db.collectionName.insertOne({object});
    - JavaScript syntax comparison
        - object.object.method({object});
*/

db.users.insertOne({
    "firstName": "John",
    "lastName": "Smith"
});

// Insert many documents
/*
    - Syntax
        - db.collectionName.insertMany([ {objectA}, {objectB} ]]);
*/
db.users.insertMany([
    {
        "firstName": "Jane",
        "lastName": "Doe"
    },
    {
        "firstName": "John",
        "lastName": "Doe"
    }
]);

// [Section] Finding documents (Read)
// Find
/*
    - If multiple documents match the criteria for finding a document only the FIRST document that matches the search term will be returned
    - This is based from the order that documents are stored in a collection
    - If a document is not found, the terminal will respond with a blank line
    - Syntax
        - db.collectionName.find();
        - db.collectionName.find({ field: value });
*/

// Finding a single document
// Leaving the search criteria empty will retrieve ALL the documents
db.users.find();

db.users.find({ "lastName": "Doe" });

// Finding documents with multiple parameters
/*
    - Syntax
        - db.collectionName.find({ fieldA: valueA, fieldB: valueB });
*/
db.users.find({ "lastName": "Doe", "firstName": "John" });

// [Section] Updating documents (Update)

// Updating a single document

/*
    - Just like the "find" method, methods that only manipulate a single document will only update the FIRST document that matches the search criteria.
    - Syntax
        - db.collectionName.updateOne( {criteria}, {$set: {field: value}});
*/
db.users.updateOne(
    { "lastName": "Smith" },
    {
        $set : {
            "lastName": "Cena"
        }
    }
);

// Updating multiple documents
/*
    - Syntax
        - db.collectionName.updateMany( {criteria}, {$set: {field: value}});
*/
db.users.updateMany(
   { "lastName": "Doe" },
   {
     $set: { "isAdmin": false }
   }
);

// [Section] Deleting documents (Delete)

// Deleting a single document
/*
    - Syntax
        - db.collectionName.deleteOne({criteria});
*/
db.users.deleteOne({});

db.users.deleteOne({ "lastName": "Doe" });


// Delete Many
/*
    - Be careful when using the "deleteMany" method. If no search criteria is provided, it will delete all documents in a database.
    - DO NOT USE: databaseName.collectionName.deleteMany()
    - Syntax
        - db.collectionName.deleteMany({criteria});
*/
db.users.deleteMany({});

db.users.deleteMany({ "isAdmin": false });
